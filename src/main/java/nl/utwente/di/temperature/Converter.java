package nl.utwente.di.temperature;

public class Converter {
    public double getBookPrice(String isbn) {
        return (double) Math.round((Double.parseDouble(isbn) * 1.8 + 32.0) * 100)/100;
    }
}
